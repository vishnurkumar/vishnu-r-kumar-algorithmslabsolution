package com.greatlearning.lab4.main;

import java.util.Scanner;

import com.greatlearning.lab4.services.BubbleSorting;
import com.greatlearning.lab4.services.NoteCount;

public class App {

	public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the size of currency denomination");
			int size = sc.nextInt();
			int notes[] = new int[size];
			System.out.println("Enter the currency denominations value");
			for(int i= 0 ; i< size ; i++) {
				notes[i] = sc.nextInt();		
				}
			BubbleSorting sort = new BubbleSorting();
			sort.bubbleSort(notes);
			
			System.out.println("Enter the amount you want to pay");
			int amount = sc.nextInt();
			sc.close();
			
			NoteCount count = new NoteCount();
			count.notesCounter(notes, amount);
		}
	}


