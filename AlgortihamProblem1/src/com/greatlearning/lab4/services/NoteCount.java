package com.greatlearning.lab4.services;

public class NoteCount {
	
	public void notesCounter(int notes[], int amount) {
		
		int noteCounter[] = new int[notes.length];
		
		for(int i = 0; i < notes.length; i++) {
			
			if(amount >= notes[i]) {
				
				noteCounter[i] = amount/notes[i];
				amount -= noteCounter[i]*notes[i];
			}
		}
		
		if(amount>0) {
			System.out.println("Exact amount can not be given with highest denomination");
		}
		else {
			System.out.println("Your payment approach is");
			for(int i=0; i<notes.length ; i++) {
				if(noteCounter[i] !=0) {
					System.out.println(notes[i] + "-" + noteCounter[i]);
				}
			}
		}
	}

}
