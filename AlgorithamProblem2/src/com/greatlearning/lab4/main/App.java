package com.greatlearning.lab4.main;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of Transaction Array");
		int sizeOfTrans = sc.nextInt();
		int numbOfTans[] = new int[sizeOfTrans];
		System.out.println("Enter the transaction amount");
		for(int i = 0 ; i < sizeOfTrans; i++ ) {
			numbOfTans[i] = sc.nextInt();
		}
		System.out.println("Enter the no of targets that need to be achieved");
		int targetNumber = sc.nextInt();
		while (targetNumber !=0) {
			int flag = 0;
			System.out.println("Enter the target amount");
			int targetAmount = sc.nextInt();
			int sum =0;
			
			for(int i =0 ; i < sizeOfTrans; i++) {
				sum += numbOfTans[i];
				if(sum>= targetAmount) {
					System.out.println("Target Achieved after " + (i+1) + " transaction done");
					flag = 1;
					break;
				}
			}
//			System.out.println("sum" + sum);
			if(flag == 0) {
				System.out.println("given target didn't achieved");
			}
			
			targetNumber --;
		}
	}

}
